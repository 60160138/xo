import java.util.*

object OX {
    var XO = arrayOf(
            arrayOf(" ", " ", " "),
            arrayOf(" ", " ", " "),
            arrayOf(" ", " ", " ")
    )
    var turn = "X"
    var R = 0
    var C = 0
    var Round = 0

    @JvmStatic
    fun main(args: Array<String>) {

        println(" Start the Game !! ")

        while (true) {
            printtable()
            inputPosition()

            if (checkWin()) {
                break
            } else if (checkDraw()) {
                break
            }
            switchInput()
        }
    }



    fun printtable() {
        println(" " + " 1 " + "2" + " 3 ")
        for (i in 0..2) {
            print(i + 1)
            for (j in 0..2) {
                print("|" + XO[i][j])
            }
            print("|")
            println()
        }
    }

    fun inputPosition() {
        val kb = Scanner(System.`in`)
        while (true) {
            println("Turn $turn")
            print(" Choose position(Roll,Column) : ")
            try {
                val a = kb.next()
                val b = kb.next()
                R = a.toInt()
                C = b.toInt()
                if (R > 3 || R < 0 || R == 0 || C > 3 || C < 0 || C == 0) {
                    println("Row and Column must be number 1-3")
                    continue
                }
                R = R - 1
                C = C - 1
                if (XO[R][C] !== " ") {
                    println("Row " + (R + 1) + " and Column " + (C + 1) + " Can't choose ")
                    continue
                }
                Round++
                XO[R][C] = turn
                break
            } catch (a: Exception) {
                println("Row and Column must be number")
                continue
            }
        }
    }

    fun checkWin(): Boolean {
        var chk = false

        for (i in XO.indices) {
            if (XO[i][0] === turn && XO[i][1] === turn && XO[i][2] === turn
            ) {
                chk = true
            }
        }

        for (i in XO.indices) {
            if (XO[0][i] === turn && XO[1][i] === turn && XO[2][i] === turn
            ) {
                chk = true
            }
        }

        if (XO[0][0] === turn && XO[1][1] === turn && XO[2][2] === turn
        ) {
            chk = true
        }
        if (XO[0][2] === turn && XO[1][1] === turn && XO[2][0] === turn
        ) {
            chk = true
        }
        if (chk == true) {
            printtable()
            println("$turn WIN")
            return true
        }
        return false
    }

    fun checkDraw(): Boolean {
        if (Round == 9) {
            printtable()
            println("DRAW")
            return true
        }
        return false
    }

    fun switchInput() {
        turn = if (turn === "X") {
            "O"
        } else {
            "X"
        }
    }
}